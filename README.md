# Debian packaging with git


## Start a new Debian package from a git checkout

Branch from the last stable tag into a Debian branch following the [DEP-14](http://dep.debian.net/deps/dep14/) specification, e.g.:

```
git branch debian/master vX.Y.Z
```

Add initial packaging templates and adjust all the needed bits:

```
dh_make --createorig -p NAME_VERSION
```

Commit the `debian/` directory into the `debian/master` branch:

```
git add debian/
git commit
```


## Preparing the package for `git-buildpackage`

Assuming that the Debian packaging is in the `debian/master` branch and that the upstream code is in the `master` branch and uses tags, a file like the following can be put in `debian/gbp.conf`:

```
[DEFAULT]
debian-branch = debian/master
upstream-branch = master
upstream-tag = v%(version)s
pristine-tar = False
```

If the upstream repository does not have tags, the `orig.tar.gz` archive could be generated from a branch using a line like the following:

```
upstream-tree=THE_BRANCH_NAME
```


## Create the build environment

```
DIST=sid ARCH=amd64 BUILDER=pbuilder git pbuilder create
```

Extra packages can be added at create-time passing them as a space-separated string to the `--extrapackages` option, like so:

```
DIST=sid ARCH=amd64 BUILDER=pbuilder git pbuilder create --extrapackages "ca-certificates"
```

To create a build environment for building packages for Ubuntu, install the `ubuntu-keyring` package, and pass the target distribution to the `DIST` env variable along with some other command line options, e.g.:

```
DIST=focal ARCH=amd64 BUILDER=pbuilder git pbuilder create --mirror http://archive.ubuntu.com/ubuntu/ --components "main restricted universe multiverse" --keyring /usr/share/keyrings/ubuntu-archive-keyring.gpg
```

For a build environment to build packages for the BeagleBone Black:

```
DIST=wheezy ARCH=armhf BUILDER=qemubuilder git pbuilder create
```

To use raspbian instead, in order to build packages for Raspberry Pi (taken from <http://info.comodo.priv.at/blog/cowbuilder_crossbuilds_for_raspbian.html>):

```
apt-get install cowbuilder
wget http://archive.raspbian.org/raspbian/pool/main/r/raspbian-archive-keyring/raspbian-archive-keyring_20120528.2_all.deb
dpkg -i raspbian-archive-keyring_20120528.2_all.deb
cowbuilder --create --basepath /var/cache/pbuilder/armhf/raspbian-wheezy-base.cow \
           --distribution wheezy --mirror http://mirrordirector.raspbian.org/raspbian/ \
           --components "main contrib non-free rpi" --extrapackages eatmydata \
           --architecture armhf \
           --debootstrap qemu-debootstrap --debootstrapopts --variant=buildd \
           --debootstrapopts --keyring=/usr/share/keyrings/raspbian-archive-keyring.gpg
```


## Modify the build environment

If there is the need to modify an existing build environment in a permanent way (e.g. install the `ca-certificate` package), this can be done interactively with this command:

```
DIST=sid ARCH=amd64 BUILDER=pbuilder git pbuilder login --save-after-login
...
# apt-get install ca-certificates
# exit
```

For example to be able to build packages with source format `3.0 (git)` the `git` package needs to be installed as well:

```
DIST=sid ARCH=amd64 BUILDER=pbuilder git pbuilder login --save-after-login
...
# apt-get install git
# exit
```


## Update the build environment

```
DIST=sid ARCH=amd64 BUILDER=pbuilder git pbuilder update
```


## Packaging a new upstream release

Tag the upstream branch as `vX.Y.Z`.

Merge the upstream branch into the `debian/master` branch

```
git checkout debian/master
git merge vX.Y.Z
```

Generate a new changelog entry from the changes to the `debian/` directory:

```
gbp dch --auto --new-version X.Y.Z-1 debian/
```

Update the `debian/` changelog:

```
gbp dch --auto -- debian/
```

and commit appending the following tag in the commit message, in order to prevent `debian/changelog` changes to show up in `debian/changelog`:

```
Gbp-Dch: ignore
```

Or if the package is already ready for release use the following command, which removes the `UNRELEASED` label and puts in the actual distro name:

```
gbp dch --auto -R -- debian/

```

For binary builds for Ubuntu a new version with a suffix may be created passing the target distribution to the `-D' option, e.g.:

```
gbp dch -D focal -l ~ubuntu20.04.
```


## Build with `git-buildpackage`

Pass the target distribution and architecture as arguments of the `--git-dist` and `--git-arch` options:

```
BUILDER=pbuilder gbp buildpackage --git-pbuilder --git-dist=sid --git-arch=amd64 -d
```

The `-d` options from above makes sure to skip the build dependencies check performed by `[p]debuild` *outside* the chroot. This way it is not necessary to install all the build dependencies on the host.

However this may not be enough in general:

> <jamessan> ao2: All that does is disable the dependency check.
> That means `debian/rules clean` will fail if it uses something from `Build-Depends` that you don't have installed outside of the chroot.

Lintian checks can be automated adding the following lines to `$HOME/.gbp.conf`:

```
[buildpackage]
postbuild = lintian $GBP_CHANGES_FILE
```

Test also binary-only builds:

```
BUILDER=pbuilder gbp buildpackage --git-pbuilder --git-dist=sid --git-arch=amd64 --git-pbuilder-options="--binary-arch" -d
```

Once a package has been successfully uploaded, a Debian version can be tagged:

```
gbp buildpackage --git-tag-only
```


## Check the package with `piuparts`

```
DIST=sid ARCH=amd64 \
eval sudo piuparts \
  --log-level dump \
  --list-installed-files \
  --pedantic-purge-test \
  --warn-on-leftovers-after-purge \
  --warn-on-others \
  -b '/var/cache/pbuilder/base-$DIST-$ARCH.tgz' ../build-area/PACKAGE_NAME.deb
```

**NOTE**: in the command above, the `eval` and the single quotes prevent the premature expansion of the env variables, this is needed because the env variables are set and then used on the same command line, and the shell does variable expansion before modifying the environment, see <http://stackoverflow.com/questions/10938483>.

If the package uses `debconf` to prompt questions to the user, the answers can be pre-set so that an automatic installation can be performed.

This can be done using a `piuparts` script (e.g. `$HOME/piuparts_scripts/pre_install_preseed`), and passing this option to `piuparts`:

```
--scriptsdir="$HOME/piuparts_scripts/"
```

For example, `pre_install_preseed` script could contain a line like the following one:

```
echo "package package/debconf_entry boolean true" | debconf-set-selections
```

Any shell command can be used in `piuparts` script.

For example, a `post_install_exec` script could launch one of the executables from the package to verify that all the run-time dependencies are available.

If the same script is meant to be used for several packages, then it has to use some mechanism to execute any command specific to a certain package only for that package, the env variable `PIUPARTS_OBJECTS` can be used for that.

See `/etc/piuparts/scripts` for more examples, but mind that when installing local packages you have to use something like this:

```
# strip the version part from local packages
# e.g.
#   ../build-area/kinect-audio-setup_0.4-2_amd64.deb
# becomes
#   ../build-area/kinect-audio-setup
PIUPARTS_OBJECTS="${PIUPARTS_OBJECTS%%_*}"
...
...
case ${PIUPARTS_OBJECTS%%=*} in
  *package_name)
```


## Check package with `cme`

See what `libconfig-model-dpkg-perl` suggests:

```
cme check dpkg
cme check dpkg-control
cme check dpkg-copyright
```

And fix it automatically:

```
cme fix dpkg-control
cme fix dpkg-copyright
```

`cme` can also help to compose the `debian/copyright` file:

```
cme update dpkg-copyright
```

However it is advised to perform manual inspection to the generated file, it's not always perfect.


## Check URLs with `duck`

```
duck
```


## Manage patches with `gbp-pq`

TBD

## Upload the package to the archive

From <https://spwhitton.name//blog/entry/pushsourcedropin/>:

```
dgit push-source --gbp
```
